<?php

/**
* Partner Custom Post Type
*/
class Promotional_Partners_Partner_CPT
{

	function __construct($post_id = null)
	{
		if (!empty($post_id)){
			$this->getPost($post_id);
		}
	}

	public function getPost($post_id)
	{
		$this->post = get_post($post_id);
	}

	public function apiInit($server)
	{
		global $promotional_partners_partner_api;

		$promotional_partners_partner_api = new Promotional_Partners_Partner_API($server);
		$promotional_partners_partner_api->register_filters();

	}

	public function init()
	{
		register_post_type(
			'partner',
			array(
				'labels' => array(
					'name' => __('Partners'),
					'singular_name' => __('Partner'),
				),
				'public' => true,
				'has_archive' => true,
				'supports' => array(
					'title',
					'editor',
					'thumbnail',
					'custom-fields'
				),
			)
		);

		register_taxonomy(
			'partner_category',
			'partner',
			array(
				'labels' => array(
					'name' => __('Partner Categories'),
					'singular_name' => __('Partner Category'),
				),
				'hierarchical' => true,
				'rewrite' => array('slug' => 'p_category'),
			)
		);

	}
}