<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * Dashboard. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           Promotional_Partners
 *
 * @wordpress-plugin
 * Plugin Name:       Promotional Partners
 * Plugin URI:        http://example.com/promotional-partners-uri/
 * Description:       Allows you to create promotional partners on your wordpress site.
 * Version:           1.0.0
 * Author:            Stephan Grobler
 * Author URI:        http://example.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       promotional-partners
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
/**
 * Include the core_plugin of the api to ensure that the api stuff is loaded
 */
require_once plugin_dir_path( __FILE__ ) . '../json-rest-api/plugin.php';

/**
 * The code that runs during plugin activation.
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/class-promotional-partners-activator.php';

/**
 * The code that runs during plugin deactivation.
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/class-promotional-partners-deactivator.php';

/** This action is documented in includes/class-promotional-partners-activator.php */
register_activation_hook( __FILE__, array( 'Promotional_Partners_Activator', 'activate' ) );

/** This action is documented in includes/class-promotional-partners-deactivator.php */
register_deactivation_hook( __FILE__, array( 'Promotional_Partners_Deactivator', 'deactivate' ) );

/**
 * The core plugin class that is used to define internationalization,
 * dashboard-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-promotional-partners.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_promotional_partners() {

	$plugin = new Promotional_Partners();
	$plugin->run();

}
run_promotional_partners();
